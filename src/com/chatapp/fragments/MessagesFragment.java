package com.chatapp.fragments;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.chatapp.R;
import com.chatapp.adapters.MessagesAdapter;
import com.chatapp.model.Message;

public class MessagesFragment extends ListFragment {
	private static final String TAG = MessagesFragment.class.getName();
	private View rootView;
	private List<Message> mMessages;
	private MessagesAdapter mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater
				.inflate(R.layout.fragment_friends, container, false);
		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		mMessages = new ArrayList<Message>();
		mAdapter = new MessagesAdapter(getActivity(),
				R.layout.message_item_layout, mMessages);
		setListAdapter(mAdapter);
		initMessages();
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Log.d(TAG, "List ite clicked");
	}

	String[] from = new String[] { "Ashish Agre", "Rucha Agre",
			"Nitesh Sharma", "Gaurav Bagdia", "Neeraj Khorgade" };
	String[] message = new String[] {
			"Here is a simple trick for Hacking Yahoo! Passwords. It?s really superb.",
			"This is a mailing address to the Yahoo Staff. The automated server will send you the ",
			"On the third line type in the password to your e-mail address ( your own password). The computer needs your password, so it can send a ______JavaScript",
			"From your account in the Yahoo! Server to extract the other e-mail addresses password.",
			"The system automatically checks your password to confirm the integrity of your status. Remember you are sending your password to a machine not to a human being" };

	private void initMessages() {
		Message m = null;
		for (int i = 0; i < message.length; i++) {
			m = new Message();
			m.setFrom(from[i]);
			m.setMessage(message[i]);
			mMessages.add(m);
		}
		mAdapter.notifyDataSetChanged();
	}
}
