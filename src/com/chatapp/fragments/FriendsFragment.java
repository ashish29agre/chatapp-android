package com.chatapp.fragments;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.chatapp.R;
import com.chatapp.adapters.FriendsAdapter;
import com.chatapp.model.Friend;

public class FriendsFragment extends Fragment {
	private static final String TAG = FriendsFragment.class.getName();
	private View rootView;
	private EditText search;
	private ListView listView;
	private FriendsAdapter mAdapter;
	private List<Friend> friends;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater
				.inflate(R.layout.fragment_friends, container, false);
		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		initView();
	}

	private void initView() {
		listView = (ListView) rootView.findViewById(R.id.list_view_friends);
		search = (EditText) rootView.findViewById(R.id.search_edittext);
		search.addTextChangedListener(new SearchTextListener());
		friends = new ArrayList<Friend>();
		mAdapter = new FriendsAdapter(getActivity(),
				R.layout.friend_item_layout, friends);
		Friend friend = null;
		for (int i = 0; i < friendsAry.length; i++) {
			friend = new Friend();
			friend.setName(friendsAry[i]);
			friend.setLogin(logins[i]);
			friends.add(friend);
		}
		listView.setAdapter(mAdapter);
	}

	private CharSequence friendsAry[] = new String[] { "Ashish Agre",
			"Rucha Agre", "Nitesh Sharma", "Gaurav bagdia", "Neeraj Khorgade" };
	private String logins[] = new String[] { "@ashish", "@rucha", "@nitesh",
			"@gaurav", "@naru" };

	private class SearchTextListener implements TextWatcher {

		@Override
		public void afterTextChanged(Editable arg0) {
		}

		@Override
		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {

		}

		@Override
		public void onTextChanged(CharSequence charSequence, int arg1,
				int arg2, int arg3) {
			mAdapter.getFilter().filter(charSequence);
			Log.d(TAG, "" + charSequence.toString());
		}
	}

}
