package com.chatapp.model;

import android.graphics.drawable.Drawable;

public class DrawerItem {
	private Drawable drawable;
	private CharSequence title;

	public Drawable getDrawable() {
		return drawable;
	}

	public void setDrawable(Drawable drawable) {
		this.drawable = drawable;
	}

	public CharSequence getTitle() {
		return title;
	}

	public void setTitle(CharSequence title) {
		this.title = title;
	}

}
