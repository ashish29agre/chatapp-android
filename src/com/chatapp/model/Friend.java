package com.chatapp.model;

import android.graphics.drawable.Drawable;

public class Friend {
	private Drawable avatar;
	private CharSequence name;
	private String login;

	public Drawable getAvatar() {
		return avatar;
	}

	public void setAvatar(Drawable avatar) {
		this.avatar = avatar;
	}

	public CharSequence getName() {
		return name;
	}

	public void setName(CharSequence name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public String toString() {
		return name != null ? name.toString() : "";
	}
}
