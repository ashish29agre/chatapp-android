package com.chatapp.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chatapp.R;
import com.chatapp.model.DrawerItem;

public class DrawerAdapter extends ArrayAdapter<DrawerItem> {
	private List<DrawerItem> mItems;
	private LayoutInflater mInflater;
	private int mResource;
	private Context mContext;

	public DrawerAdapter(Context context, int resource,
			List<DrawerItem> drawerItems) {
		super(context, resource, drawerItems);

		this.mContext = context;
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mItems = drawerItems;
		this.mResource = resource;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		DrawerHolder holder;
		final DrawerItem item = mItems.get(position);
		if (convertView == null) {
			convertView = mInflater.inflate(mResource, parent, false);
			holder = new DrawerHolder();
			convertView.setTag(holder);
		} else {
			holder = (DrawerHolder) convertView.getTag();
		}
		holder.dImage = (ImageView) convertView
				.findViewById(R.id.drawer_item_img);
		holder.dText = (TextView) convertView
				.findViewById(R.id.drawer_item_text);
		holder.dImage.setImageDrawable(item.getDrawable());
		holder.dText.setText(item.getTitle());
		return convertView;
	}

	private static class DrawerHolder {
		private ImageView dImage;
		private TextView dText;
	}
}
